import React from 'react';

// Creates a context object
const UserContext = React.createContext();

// Provider Component that allows us to provide the context object with states and function
export const UserProvider = UserContext.Provider;

export default UserContext;