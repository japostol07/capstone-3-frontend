import React from 'react';
import Link from 'next/link';
import { Button } from 'react-bootstrap';

export default function Category({category}) {
	return (
		<tr>
			<td> { category.categoryName } </td>
			<td> { category.categoryType } </td>
			<td>

				<Link href={`/deleteCategory?categoryId=${category._id}`}>
					<Button variant="danger">Delete</Button> 
				</Link>

			</td>
		</tr>	
	)
}