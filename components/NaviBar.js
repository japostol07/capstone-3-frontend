import React, { useContext } from 'react';
import Link from 'next/link';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NaviBar() {
	
	const { user } = useContext(UserContext);

	console.log({user})

	return (
		<Navbar bg="dark" expand="lg" className="text-light" >
			{(user.token == null) ?
		  	<Link href="/">
		  		<a className="navbar-brand text-light">Budget Cracker</a>
		  	</Link>
		  	:
		  	<Link href="/records">
		  		<a className="navbar-brand text-light">Budget Cracker</a>
		  	</Link>
			}
		 	<Navbar.Toggle aria-controls="basic-navbar-nav" />
		  		<Navbar.Collapse id="basic-navbar-nav">
		    		<Nav className="ml-auto">
		    			{(user.token == null) ?
		    			<React.Fragment>
		    			</React.Fragment>
		      			:
		      			<React.Fragment>
		      				<Link href="/records">
					  			<a className="nav-link text-light" role="button">Records</a>
					  		</Link>

					      	<Link href="/categories">
					  			<a className="nav-link text-light" role="button">Categories</a>
					  		</Link>
					      
					      	<Link href="/incomePieChart">
					  			<a className="nav-link text-light" role="button">Income Breakdown</a>
					  		</Link>

					  		<Link href="/expensePieChart">
					  			<a className="nav-link text-light" role="button">Expense Breakdown</a>
					  		</Link>
					      	
						    <Link href="/incomeLineGraph">
					  			<a className="nav-link text-light" role="button">Income Trend</a>
					  		</Link>

					  		<Link href="/expenseLineGraph">
					  			<a className="nav-link text-light" role="button">Expense Trend</a>
							</Link>

							<Link href="/profile">
					  			<a className="nav-link text-light" role="button">Profile</a>
							</Link>
						
							<Link href="/logout">
					  			<a className="nav-link text-light" role="button">Logout</a>
							</Link>
						</React.Fragment>
		      			}
		  			</Nav>
		     	</Navbar.Collapse>
		</Navbar>
	)
}