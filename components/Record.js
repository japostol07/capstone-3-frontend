import React from 'react';
// import { Button, Table } from 'react-bootstrap';
import moment from 'moment';
import Link from 'next/link';
import { Button } from 'react-bootstrap';

export default function Record({record}) {

	const date = moment(record.createdOn).format('MMM DD, YYYY')

	return (
		<tr>
			<td> { date } </td>
			<td> { record.recordType } </td>
			<td> { record.recordName } ( { record.recordDescription } ) </td>
			{ (record.recordType == "Income") ?
			<td>+ ₱ { record.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") } </td>
			:
			<td>- ₱ { record.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") } </td>
			}
			<td>
				<Link href={`/editRecord?recordId=${record._id}`}>
					<Button variant="warning">Edit</Button>
				</Link>
			</td>
			<td>	
				<Link href={`/deleteRecord?recordId=${record._id}`}>
					<Button variant="danger">Delete</Button> 
				</Link>
				
			</td>
		</tr>
		
	)
}