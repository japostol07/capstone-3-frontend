import React, { useContext, useState, useEffect } from 'react';
import AppHelper from '../../app-helper';
import Record from '../../components/Record';
import Head from 'next/head';
import { Button, Table, Form } from 'react-bootstrap';
import Link from 'next/link';


export default function index() {

	const [records, setRecords] = useState([]);
	const [balance, setBalance] = useState('');
	const [recordLength, setRecordLength] = useState('');
	const [term, setTerm] = useState('');
	const [filter, setFilter] = useState('');

	useEffect (() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data=>{

			setBalance(data.balance)
		})

	} ,[])

	useEffect (() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/records`, options)
        .then(AppHelper.toJSON)
        .then(data=>{
        	setRecordLength(data.length)
			setRecords(data)
		})

	} ,[])


	let recordData = records.map((data) => {   

		if (filter === "Income" && data.recordType === filter){
			if (term !== '') {
				         																						
				if (data.recordName.toLowerCase().includes(term.toLowerCase()) || data.recordDescription.toLowerCase().includes(term.toLowerCase())){
					return <Record key= {data._id} record= {data} />       									
				} else {
					return null
				}
			} else {
				return <Record key= {data._id} record= {data} />     											
			}

		} else if (filter === "Expense" && data.recordType === filter){
			if (term !== '') {
				         																						
				if (data.recordName.toLowerCase().includes(term.toLowerCase()) || data.recordDescription.toLowerCase().includes(term.toLowerCase())){
					return <Record key= {data._id} record= {data} />       									
				} else {
					return null
				}
			} else {
				return <Record key= {data._id} record= {data} />     											
			}

		} else if (filter !== "Income" && filter !== "Expense"){
			if (term !== '') {
				         																						
				if (data.recordName.toLowerCase().includes(term.toLowerCase()) || data.recordDescription.toLowerCase().includes(term.toLowerCase())){
					return <Record key= {data._id} record= {data} />       									
				} else {
					return null
				}
			} else {
				return <Record key= {data._id} record= {data} />     											
			}
		}

	})
	

	return(
		
		<React.Fragment>
			<Head>
				<title>Records</title>
			</Head>

			<h1 className="text-center">Records</h1>

			<Form.Group controlId="search" className = "w-25">
	            <Form.Control 
	                type="text"
	                placeholder="Search Record"
	                onChange={(e) => setTerm(e.target.value)}
	            />
	        </Form.Group>

	       <select
				className = "w-25"
				onChange={(e) => setFilter(e.target.value)}
			>
				<option value="">All</option>
				<option value="Income">Income</option>
				<option value="Expense">Expense</option>
			</select>
			
			<h4 className="text-center"> Current Balance : ₱ {balance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} </h4>

	       <Link href="/addRecord">
		  		<Button variant="primary">Add Record</Button>
		  	</Link>
		  	
		  	{(recordLength === 0) ?
		  		<Table stripe bordered hover className="text-center">
	           		<thead>
						<tr>
							<p>
								<h5 className="text-center">No Records to Display</h5> Add Categories first before adding Records
							</p>
							<p>
								<Link href="/categories">
			  						<a> Click here to add Categories</a>
			  					</Link>
		  					</p>
						</tr>
					</thead>
				</Table>
			:
				<Table stripe bordered hover className="text-center">
	           		<thead>
						<tr>
							<th>Date</th>
							<th>Type</th>
							<th>Details</th>
							<th>Amount</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{recordData}
					</tbody>
				</Table>
			}
		</React.Fragment>
	)
	
}