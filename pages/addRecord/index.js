import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import UserContext from '../../UserContext';
import { Form, Button } from 'react-bootstrap';
import RecordNameList from '../../components/RecordNameList';

export default function index() {

	const { user } = useContext(UserContext);

	const [recordType, setRecordType] = useState('');
	const [recordName, setRecordName] = useState('');
	const [recordDescription, setRecordDescription] = useState('');
	const [amount, setAmount] = useState('');

	const [nameListData, setNameListData] = useState([])
	const [nameList, setNameList] = useState([]);

	const [isActive, setIsActive] = useState(false);

	function recordAdd (e) {

		e.preventDefault()

		const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${ user.token }`},
            body: JSON.stringify({
                recordType: recordType,
                recordName: recordName,
				recordDescription: recordDescription,
				amount: amount
            })
        }
        fetch(`${AppHelper.API_URL}/records`, options)
        .then(AppHelper.toJSON)
        .then(data => {

        	if (data === true) {
				Swal.fire("Record Successfully Added");
				Router.push('/records')
			} else {
				Swal.fire('Error')
			}
        })
	}

	useEffect(() => {

		//Validation to enable submit button when field/s are populated 
		if(recordType !== "" && recordName !== "" && recordDescription !== "" && amount !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [recordType, recordName, recordDescription, amount])


	useEffect(() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/categories`, options)
        .then(AppHelper.toJSON)
        .then(data=>{

        	console.log(data)

        	setNameList(data)
		})


	}, [])

	
	useEffect(() => {

	 	let nameListMap = nameList.map((data) => {                                          // "nameList" from the nameList.map is the state
	 		
	 		console.log(data)

			if(data.categoryType == recordType ){	
				return <RecordNameList key= {data._id} recordNameList= {data} />     		// "recordNameList" in green is a prop that came from "recordNameList" in orange in RecordNameList.js line 4
			
			} else {
				return null;
			}
		})

	 	console.log(nameListMap)

	 	setNameListData(nameListMap)

	} ,[recordType])


	return(
		<React.Fragment>
			<Head>
				<title>Add Record</title>
			</Head>
			<h2 className="text-center">Add Record</h2>
			<Form onSubmit={(e) => recordAdd(e)}>
				<Form.Group>
					<Form.Label>Category Type</Form.Label>
					<div>
						<select
							className="w-25"
							value={recordType}
							onChange={(e) => setRecordType(e.target.value)}
							required
						>
							<option value="">Choose Type</option>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
						
						</select>
					</div>
				</Form.Group>

				<Form.Group>
					<Form.Label>Category Name</Form.Label>
					<div>
						<select
							className="w-25"
							value={recordName}
							onChange={(e) => setRecordName(e.target.value)}
							required
						>
							<option value="">Choose Name</option>
							{ nameListData }
						</select>
					</div>
				</Form.Group>

				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Description "
						value={recordDescription}
						onChange={(e) => setRecordDescription(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Transaction Amount</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Amount"
						value={amount}
						onChange={(e) => setAmount(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}
			</Form>
			
		</React.Fragment>
	)
}