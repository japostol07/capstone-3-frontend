import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';

export default function index() {

	const { user } = useContext(UserContext);

	const [userId, setUserId] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');

	useEffect(() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data=>{

 			setUserId(data._id)
			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
		})
		
	}, [])

	function editUser(e){

		e.preventDefault();

		const option = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${ user.token }`},
            body: JSON.stringify({
                email: email
            })
        }
		fetch(`${AppHelper.API_URL}/users/emailExistsEditProfile`, option)
        .then(AppHelper.toJSON)
        .then(data => {

        	if(data === false){

				const options = {
		            method: 'PUT',
		            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${ user.token }`},
		            body: JSON.stringify({
		            	userId: userId,
		                firstName: firstName,
		                lastName: lastName,
		                email: email
		            })
		        }
				fetch(`${AppHelper.API_URL}/users`, options)
		        .then(AppHelper.toJSON)
		        .then(data => {

		        	if(data === true){
		        		Swal.fire('User Information Successfully Updated!')
		        		Router.push('/profile')
		        	} else {
		        		Swal.fire('Error')
		        	}
				})

	    	} else {
        		Swal.fire('The email address is already in use')
        	}
		})
	
	}

	return(
		<React.Fragment>
			<Head>
				<title>Edit Profile</title>
			</Head>
			<h3 className="text-center"> Edit User Information </h3>
			<Form onSubmit={(e) => editUser(e)}>
				<Form.Group >
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder={firstName}
						value={firstName}
						onChange={(e) => setFirstName(e.target.value)}
						required
						/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder={lastName}
						value={lastName}
						onChange={(e) => setLastName(e.target.value)}
						required
						/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control
						type="email"
						placeholder={email}
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
						/>
				</Form.Group>

				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			</Form>
		</React.Fragment>
	)
}