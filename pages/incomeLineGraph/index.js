import React, { useState, useEffect } from 'react';
import AppHelper from '../../app-helper';
import Head from 'next/head';    //for <title>
import { Form, Table, Row, Col } from 'react-bootstrap';
import { Line } from 'react-chartjs-2';
import moment from 'moment';

export default function index () {

	const [lineData, setLineData] = useState([]);
	const [income, setIncome] = useState([]);
	const [date, setDate] = useState([]);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	useEffect(() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/records`, options)
        .then(AppHelper.toJSON)
        .then(data=>{
        	console.log(data)

        	const filteredEntries = []

        	data.forEach(entry => {
            	if (entry.createdOn >= startDate && entry.createdOn <= endDate){
               		
               		filteredEntries.push(entry)
            	}
        	})
        	if(startDate !== '' && endDate !== ''){
				
				setLineData(filteredEntries)
			
			} else {

				setLineData(data)
			}
		})

	}, [startDate, endDate])

	useEffect (() => {

		let date = []

		lineData.forEach(entry => {
			if(date.indexOf(moment(entry.createdOn).format('DDMMMYY')) === -1 && entry.recordType === "Income"){
				date.push(moment(entry.createdOn).format('DDMMMYY'))
			}	
		})
		console.log(date)

		setDate(date)
		

	} ,[lineData])

	useEffect(() => {

		setIncome(date.map(dates => {
			let income = 0

				lineData.forEach((entry) => {
					
					if(dates === moment(entry.createdOn).format('DDMMMYY') && entry.recordType === "Income"){
					    
					    income += parseInt(entry.amount)
				       }
					})

					return income;
		}))

	} , [date])


	const data = {
		labels: date,
		datasets: [
			{
				label: 'Income Trend',
				backgroundColor: 'rgba(175,47,79,0.4)',
				data: income
			}
		]
	}

	return (
		<React.Fragment>
			<Head>
				<title>Income Line Chart</title>
			</Head>
			<Row>
				<Col>
					<Form.Group controlId="startDate">
						<Form.Label>Set Start Date</Form.Label>
						<Form.Control
							type="date"
							onChange={(e) => setStartDate(e.target.value)}
						/>
					</Form.Group>
				</Col>
				<Col>
					<Form.Group controlId="endDate">
						<Form.Label>Set End Date</Form.Label>
						<Form.Control
							type="date"
							onChange={(e) => setEndDate(e.target.value)}
						/>
					</Form.Group>
				</Col>
			</Row>
			<Line data={data} />
		</React.Fragment>
	)
}