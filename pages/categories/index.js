import React, { useContext, useState, useEffect } from 'react';
import AppHelper from '../../app-helper';
import Category from '../../components/Category';
import Head from 'next/head';    //for <title>
import { Button, Table } from 'react-bootstrap';
import Link from 'next/link';


export default function index() {

	const [categories, setCategories] = useState([]);
	const [categoryLength, setCategoryLength] = useState('');
	
	useEffect(() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL}/categories`, options)
        .then(AppHelper.toJSON)
        .then(data=>{

        	setCategoryLength(data.length)

        	setCategories(data)
		})

	}, [])

	const categoryData = categories.map((data) => {              // "categories" from the categories.map is the state
		return <Category key= {data._id} category= {data} />     // "category" in green is a prop that came from "category" in orange in Category.js line 4
	})


    return (

		<React.Fragment>
			<Head>
				<title>Categories</title>
			</Head>

			<h1 className="text-center">Categories</h1>

			<Link href="/addCategory">
		  		<Button variant="primary">Add Category</Button>
		  	</Link>

		  	{ (categoryLength === 0) ?
				<Table stripe bordered hover className="text-center">
	           		<thead>
						<tr>
							<h5 className="text-center">No Categories to Display</h5> Add Categories first before adding Records
						</tr>
					</thead>
				</Table>
			:
				<Table stripe bordered hover className="text-center">
					<thead>
						<tr>
							<th>Category Name</th>
							<th>Category Type</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{categoryData}
					</tbody>
				</Table>
			}
		</React.Fragment>
	)

}