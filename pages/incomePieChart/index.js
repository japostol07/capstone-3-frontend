import React, { useState, useEffect } from 'react';
import AppHelper from '../../app-helper';
import Head from 'next/head';    //for <title>
import { Form, Table, Row, Col } from 'react-bootstrap';
import { Pie } from 'react-chartjs-2';
import getRandomColor from 'randomcolor';
//import PieComponent from '../../components/PieComponent';

export default function index () {

	const [pieData, setPieData] = useState([]);
	const [description, setDescription] = useState([]);
	const [amount, setAmount] = useState([]);
	const [bgColors, setBgColors] = useState ([]);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	useEffect(() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/records`, options)
        .then(AppHelper.toJSON)
        .then(data=>{
        	console.log(data)

        	const filteredEntries = []

        	data.forEach(entry => {
            	if (entry.createdOn >= startDate && entry.createdOn <= endDate){
               		
               		filteredEntries.push(entry)
            	}
        	})
        	if(startDate !== '' && endDate !== ''){
				
				setPieData(filteredEntries)
			
			} else {

				setPieData(data)
			}
		})

	}, [startDate, endDate])

	useEffect(() => {
		let description = []

		pieData.forEach((entry) => {
			if(description.indexOf(entry.recordName) === -1 && entry.recordType === "Income"){
	        	description.push(entry.recordName)
	        }
	    })

	    setDescription(description)

	}, [pieData])

	useEffect (() => {
		setAmount(description.map((data) => {
			let amounts = 0

			pieData.forEach((entry) => {

				if(entry.recordName === data && entry.recordType === "Income"){
		        	 amounts += parseInt(entry.amount)
				}
 			})
				return amounts;
		   
		}))

		setBgColors (description.map(() => getRandomColor()))

	},[description])


	const data = {
	   labels: description,
	    datasets: [
	        {
	            data: amount,
	            backgroundColor: bgColors
	        }
	    ]
	}

	return (
		<React.Fragment>
			<Head>
				<title>Income Breakdown</title>
			</Head>
			<Row>
				<Col>
					<Form.Group controlId="startDate">
						<Form.Label>Set Start Date</Form.Label>
						<Form.Control
							type="date"
							onChange={(e) => setStartDate(e.target.value)}
						/>
					</Form.Group>
				</Col>
				<Col>
					<Form.Group controlId="endDate">
						<Form.Label>Set End Date</Form.Label>
						<Form.Control
							type="date"
							onChange={(e) => setEndDate(e.target.value)}
						/>
					</Form.Group>
				</Col>
			</Row>
			<Pie data={data} />
		</React.Fragment>
	)
}