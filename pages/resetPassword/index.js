import React, { useState, useEffect, useContext }from 'react';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';

export default function index() {

	const { user } = useContext(UserContext);

	const [userId, setUserId] =  useState('');
	const [currentPassword, setCurrentPassword] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState('');

	useEffect(() => {

		const option = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/users/details`, option)
        .then(AppHelper.toJSON)
        .then(data=>{

 			setUserId(data._id)
		})

	}, [])

	
	function resetPassword(e){

		e.preventDefault();

		const option = {
			method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
            	userId: userId,
                password: currentPassword
            })
        }
		fetch(`${ AppHelper.API_URL }/users/comparePassword`, option)
        .then(AppHelper.toJSON)
        .then(data=>{ 

        	if(data === true){
				const options = {
					method: 'PUT',
		            headers: {'Content-Type': 'application/json', Authorization: `Bearer ${ user.token }`},
		            body: JSON.stringify({
		            	userId: userId,
		                password: password
		            })
		        }
				fetch(`${ AppHelper.API_URL }/users/resetPassword`, options)
		        .then(AppHelper.toJSON)
		        .then(data=>{

		        	if(data === true){
						Swal.fire("Your password has been reset!")
					    Router.push('/profile')

				    } else{
						Swal.fire('Error');

					}
				})

			} else {
				Swal.fire('Current Password is Incorrect', '','error')
			}
		})
	}

	useEffect(() => {

		if(password === password2 && password !== '' && password2 !== '' && currentPassword !== ''){
			setIsActive(true)

		} else {
			setIsActive(false)
		}

	}, [password, password2])

	return(
		<React.Fragment>
			<Head>
				<title>Reset Password</title>
			</Head>
			<h3 className="text-center"> Reset Password </h3>

			<Form onSubmit={(e) => resetPassword(e)}>
				<Form.Group >
					<Form.Label>Current Password</Form.Label>
					<Form.Control
						type="password"
						value={currentPassword}
						onChange={(e) => setCurrentPassword(e.target.value)}
						required
						/>
				</Form.Group>

				<Form.Group >
					<Form.Label>New Password</Form.Label>
					<Form.Control
						type="password"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Confirm New Password</Form.Label>
					<Form.Control
						type="password"
						value={password2}
						onChange={(e) => setPassword2(e.target.value)}
						/>
				</Form.Group>

				{isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}
			</Form>
		</React.Fragment>
	)
}