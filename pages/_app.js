import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NaviBar from '../components/NaviBar';
import { UserProvider } from '../UserContext';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {


	// State hook for user details
	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
		token: null
	})

	// Function for clearing the localStorage (logout)
	const unsetUser = () => {
		localStorage.clear();

		// Changes the value of the user state back to its original value
		setUser({ 
			id: null,
			isAdmin: null,
			token: null
		});
	}

	console.log(user);

	useEffect(() => {

		setUser({
			// email: localStorage.getItem('email'),
			// Will convert the string data type into boolean
			token: localStorage.getItem('token'),
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})

	}, [])

  return (
  	<React.Fragment>
  		<UserProvider value={{user, setUser, unsetUser}}>
  		<NaviBar />
  		<Container className = "my-5">
  			<Component {...pageProps} />
  		</Container>
  		</UserProvider>
  	</React.Fragment>

  	)
}

export default MyApp
