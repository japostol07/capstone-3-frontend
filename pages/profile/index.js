import React, { useState, useEffect } from 'react';
import AppHelper from '../../app-helper';
import { Table, Button } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';

export default function index(){

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ balance, setBalance ] = useState('');
	const [buttonCondition, setButtonCondition] = useState('');
	
	useEffect(() => {

		const options = {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data=>{
       
        	setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setBalance(data.balance)
		})

	} ,[])

	useEffect(() => {

		if(localStorage.getItem('loginType') === 'email'){
			setButtonCondition(true)
		}else{
			setButtonCondition(false)
		}

	}, [])

	return(
		<React.Fragment>
			<Head>
				<title>Profile Page</title>
			</Head>
			<h3 className="text-center"> User Information </h3>
			<Table stripe bordered hover className="text-center w-85">
	           	<tbody>
	           		<tr>
						<th>Name : </th>
						<td>{firstName} {lastName}</td>
					</tr>
					<tr>
						<th>Email :</th>
						<td>{email}</td>
					</tr>
					<tr>
						<th>Current Balance :</th>
						<td>₱ {balance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
					</tr>
				</tbody>		
			</Table>

			{buttonCondition ?
			<Link href="/editProfile">
				<Button variant="primary"> Edit Profile </Button>
			</Link>
			:
			null
			}

			{buttonCondition ? 
			<Link href="/resetPassword">
				<Button variant="primary"> Reset Password </Button>
			</Link>
			:
			null
			}
			
		</React.Fragment>
	)
}