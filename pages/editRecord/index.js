import React, { useEffect, useState, useContext } from 'react';
import { useRouter } from 'next/router';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';


export default function index ()  {
  
 	const router = useRouter();
 	const { user } = useContext(UserContext);

 	const [recordName, setRecordName] = useState('');
 	const [recordType, setRecordType] = useState('');
 	const [recordDescription, setRecordDescription] = useState('');
 	const [amount, setAmount] = useState('');


 	useEffect(() => {
 	
 		const options = {
 			headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
 		}

 		fetch(`${ AppHelper.API_URL }/records/${ router.query.recordId }`, options)
        .then(AppHelper.toJSON)
        .then(data=>{

        	setRecordName(data.recordName)
        	setRecordType(data.recordType)
        	setRecordDescription(data.recordDescription)
        	setAmount(data.amount)
		})

 	}, [router.query.recordId])


 	function recordEdit(e){

 		e.preventDefault();

 		const options = {
 			method: 'PUT',
 			headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${ user.token }`},
 			body: JSON.stringify ({
 				recordDescription: recordDescription,
 				amount: amount
 			})
 		}

 		fetch(`${ AppHelper.API_URL }/records/${ router.query.recordId }`, options)
        .then(AppHelper.toJSON)
        .then(data=>{

        	if (data === true) {
				Swal.fire('Record Updated');
				Router.push('/records')
			} else {
				Swal.fire('Error')
			}
        })

    }

  	return (
  		<React.Fragment>
  			<Head>
  				<title>Edit Record</title>
  			</Head>

  			<h3 className="text-center">Edit Record</h3>

  			<Form onSubmit={(e) => recordEdit(e)}>

  				<Form.Group>
  					<Form.Label> Record Name </Form.Label>
  					<Form.Control
  						type="text"
  						value={recordName}
  						disabled
  					/>
  				</Form.Group>

  				<Form.Group>
  					<Form.Label> Record Type </Form.Label>
  					<Form.Control
  						type="text"
  						value={recordType}
  						disabled
  					/>
  				</Form.Group>

  				<Form.Group>
  					<Form.Label> Description </Form.Label>
  					<Form.Control
  						type="text"
  						placeholder={recordDescription}
  						value={recordDescription}
  						onChange={(e) => setRecordDescription(e.target.value)}
  					/>
  				</Form.Group>

  				<Form.Group>
  					<Form.Label> Amount </Form.Label>
  					<Form.Control
  						type="text"
  						placeholder={amount}
  						value={amount}
  						onChange={(e) => setAmount(e.target.value)}
  					/>
  				</Form.Group>

  				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
  			</Form>
  		</React.Fragment>
  	)
}