import React, { useState, useEffect } from 'react';
import Head from 'next/head';    //for <title>
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import { Form, Button } from 'react-bootstrap';

export default function index (){

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {

		// Prevents page redirection upon form submission
		e.preventDefault();

		const option = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({
                email: email
            })
        }
		fetch(`${AppHelper.API_URL}/users/email-exists`, option)
        .then(AppHelper.toJSON)
        .then(data => {

        	if(data === false){

				const options = {
		            method: 'POST',
		            headers: { 'Content-Type': 'application/json'},
		            body: JSON.stringify({
		                firstName: firstName,
		                lastName: lastName, 
		                email: email,
		                password: password1
		            })
		        }
		        fetch(`${AppHelper.API_URL}/users`, options)
		        .then(AppHelper.toJSON)
		        .then(data => {

		        	if (data === true) {

						Swal.fire("Registered Successfully");
						Router.push('/')

					} else {

						Swal.fire("Something went wrong.")
					}
		        })

        	} else {
        		Swal.fire('The email address is already in use')
        	}
        })
	}

	useEffect(() => {

		//Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !=="") && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstName, lastName, email, password1, password2])

	return(
		<React.Fragment>
			<Head>
	            <title>Registration</title>
	         </Head>
	         <h4>Create New Account</h4>
			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="userFirstName">
					<Form.Label>FirstName</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter First Name"
						value={firstName}
						onChange={(e) => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="userLastName">
					<Form.Label>LastName</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Last Name"
						value={lastName}
						onChange={(e) => setLastName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter Email"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter Passwsord"
						value={password1}
						onChange={(e) => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

					<Form.Group controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={(e) => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}
				
			</Form>
		</React.Fragment>
	)
}