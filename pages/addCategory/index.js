import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import UserContext from '../../UserContext';
import { Form, Button } from 'react-bootstrap';

export default function index() {

	const { user } = useContext(UserContext);

	const [categoryName, setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('');
	const [isActive, setIsActive] = useState(false);


	function categoryAdd (e) {

		// Prevents page redirection upon form submission
		e.preventDefault();

		 const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${ user.token }`},
            body: JSON.stringify({
                categoryName: categoryName,
				categoryType: categoryType
            })
        }
        fetch(`${AppHelper.API_URL}/categories`, options)
        .then(AppHelper.toJSON)
        .then(data => {

        	if (data === true) {
				Swal.fire('Category Successfully Added');
				Router.push('/categories')
			} else {
				Swal.fire('Error')
			}
        })
	}

	useEffect(() => {

		//Validation to enable submit button when field/s are populated 
		if(categoryName !== "" && categoryName !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [categoryName, categoryType])

	return(
		<React.Fragment>
			<Head>
				<title>Add Category</title>
			</Head>
			<h2 className="text-center">Add Category</h2>
			<Form onSubmit={(e) => categoryAdd(e)}>
				<Form.Group controlId="userFirstName">
					<Form.Label>Category Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="e.g. Salary, Groceries etc."
						value={categoryName}
						onChange={(e) => setCategoryName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Category Type</Form.Label>
					<div>
						<select
							className="w-25"
							value={categoryType}
							onChange={(e) => setCategoryType(e.target.value)}
							required
						>
							<option value="">Choose Type</option>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
							
						</select>
					</div>
				</Form.Group>

				{isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}
			</Form>
		</React.Fragment>
	)
}