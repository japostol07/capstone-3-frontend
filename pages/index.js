import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import AppHelper from '../app-helper';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Link from 'next/link';


export default function index() {
    return (
        <React.Fragment>
            <Head>
                <title> Budget Cracker | Login </title>
            </Head>
            <Row className='justify-content-center'>
                <Col xs md="6">
                    <h3 className="text-center">Login</h3>
                        <LoginForm />
                    </Col>
            </Row>
        </React.Fragment>
    )
}

const LoginForm = () => {

    // console.log(usersData);
    const { user, setUser } = useContext(UserContext); 

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        }
        fetch(`${AppHelper.API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
                // Router.push('/courses')
            }else {
                if(data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist', 'error')
                }else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect','error')
                }else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try the alternative login procedures.', 'error')
                }
            }
        })

    }


    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);


    const authenticateGoogleToken = (response)=>{
        console.log(response)

        const payload = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({ tokenId: response.tokenId})
        }

        fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {
            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token' , data.accessToken)
                retrieveUserDetails(data.accessToken)
            }else {
                if(data.error == 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error')
                }else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error')
                }
            }
        })
    }

    //   const failed = (response)=>{
    //     console.log(response)
    // }

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }`}
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data=>{
            setUser({id: data._id, isAdmin: data.isAdmin, token: accessToken})
            localStorage.setItem('loginType', data.loginType)
            Router.push('/records')
        })
    }

    return (
        <React.Fragment>
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter your Email Address"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Enter your Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {isActive ? 

                    <Button variant="primary" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
                        Submit
                    </Button>
                    : 
                    
                    <Button variant="primary" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
                        Submit
                    </Button>

                }
                    <GoogleLogin 
                        //clientId = OAuthClient id from Cloud Google developer platform
                        clientId="1077853327260-pfjbjs0bbmjql835coul697n2kquieqp.apps.googleusercontent.com"
                        buttonText="Login with Google "
                        //onSuccess = it returns a funtion w/c returns a google user object which provides access to all of the google user method and details
                        onSuccess={ authenticateGoogleToken } 
                        onFailure={ authenticateGoogleToken }  // alternatice { failed }
                        //cookiePolicy - determines cookie policy for the origin of the google login request
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />

                    <Link href="/register">
                        <Button variant="success" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
                            Create New Account
                        </Button>
                    </Link>
            </Form>
        </React.Fragment>
    )
}


