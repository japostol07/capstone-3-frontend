import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import Head from 'next/head';

export default function index ()  {
	const router = useRouter();
	console.log(router.query.recordId);


	useEffect(() => {

		const options = {
			method: 'DELETE',
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        }

        fetch(`${ AppHelper.API_URL }/records/${ router.query.recordId }`, options)
        .then(AppHelper.toJSON)
        .then(data=>{

        	if (data === true) {
				Swal.fire('Record Deleted');
				Router.push('/records')
			} else {
				Swal.fire('Error')
			}
		})

	}, [])


	return (
		<React.Fragment>
			<Head>
				<title>Delete Record</title>
			</Head>
	  		<h1>Deleting...</h1>
  		</React.Fragment>
  	)
}